import React from 'react';
import { CarouselCircleIcon } from 'dgx-svg-icons';

/**
  * @desc Verifies that the label value exists and returns it. Otherwise, returns null.
  * @param {Object} param - Object containing the nested label
  * @param {String} param - String representation of the language property name.
  * @returns {String} param - Returns the string value for the given property match.
  */
function getLabelValue(obj = {}, lang, defaultValue) {
  return (obj && obj[lang] && obj[lang].text) ? obj[lang].text : `${defaultValue + 1}`;
}

const NavButton = ({
  className,
  handleOnTouchStart,
  handleOnTouchEnd,
  handleOnClick,
  label,
  lang,
  currentIndex,
  position,
}) => (
  <button
    className={`${className}${(currentIndex === position) ? ' active' : ''}`}
    onTouchStart={handleOnTouchStart}
    onTouchEnd={handleOnTouchEnd}
    onClick={handleOnClick}
  >
    <span className={`${className}-label`}>
      {getLabelValue(label, lang, position)}
    </span>
    <CarouselCircleIcon
      className={`${className}-icon`}
      type={(currentIndex === position) ? 'solid' : null}
    />
  </button>
);

NavButton.propTypes = {
  className: React.PropTypes.string,
  handleOnTouchStart: React.PropTypes.func,
  handleOnTouchEnd: React.PropTypes.func,
  handleOnClick: React.PropTypes.func,
  label: React.PropTypes.object,
  lang: React.PropTypes.string,
  currentIndex: React.PropTypes.number,
  position: React.PropTypes.number,
};

NavButton.defaultProps = {
  className: 'navButton',
  lang: 'en',
};

export default NavButton;
