import React from 'react';
import Swipeable from 'react-swipeable';
import {
  throttle as _throttle,
  map as _map,
  isEmpty as _isEmpty,
  extend as _extend,
} from 'underscore';
// Carousel Individual Components
import ImageItem from './../ImageItem/ImageItem.jsx';
import NavButton from './../NavButton/NavButton.jsx';
import TextItem from './../TextItem/TextItem.jsx';

const MIN_INTERVAL = 500;

class CarouselComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: props.startIndex,
      offsetPercentage: 0,
      viewportWidth: 0,
      style: {},
    };

    this.slideLeft = _throttle(this.slideLeft, MIN_INTERVAL);
    this.slideRight = _throttle(this.slideRight, MIN_INTERVAL);
    this.handleResize = this.handleResize.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleOnSwiped = this.handleOnSwiped.bind(this);
  }

  componentDidMount() {
    window.setTimeout(() => { this.handleResize(); }, MIN_INTERVAL);
    window.addEventListener('keydown', this.handleKeyDown);
    window.addEventListener('resize', this.handleResize);

    if (this.props.autoPlay) {
      this.play();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
    window.removeEventListener('resize', this.handleResize);

    if (this.intervalId) {
      window.clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }

  /**
   * @desc Obtains the proper alignment class name for each individual slide based
   * on the index, current carousel index and infinite rotation.
   * @param {number} represents the index number for the slide.
   * @param {number} represents current active index for the carousel.
   * @return {string} left, center or right string assignment.
   */
  getAlignmentClassName(index, currentIndex) {
    const LEFT = 'left';
    const CENTER = 'center';
    const RIGHT = 'right';
    let alignment;

    switch (index) {
      case (currentIndex - 1):
        alignment = ` ${LEFT}`;
        break;
      case (currentIndex):
        alignment = ` ${CENTER}`;
        break;
      case (currentIndex + 1):
        alignment = ` ${RIGHT}`;
        break;
      default:
        alignment = '';
    }

    if (this.getTotalSlidesCount() >= 3 && this.props.infinite) {
      if (index === 0 && currentIndex === this.getTotalSlidesCount() - 1) {
        // set first slide as right slide if were sliding right from last slide
        alignment = ` ${RIGHT}`;
      } else if (index === this.getTotalSlidesCount() - 1 && currentIndex === 0) {
        // set last slide as left slide if were sliding left from first slide
        alignment = ` ${LEFT}`;
      }
    }

    return alignment;
  }

  /**
   * @desc Obtains the correct CSS animation rule computed by the slide index
   * and current carousel index. If infinite is selected, it also ensures to set
   * a proper animation value to the last & first slide.
   * @param {number} represents the index number for the slide.
   * @param {number} represents current active index for the carousel.
   * @param {string} represents the type of animation to be computed.
   * @return {object} contains style object with animation rules.
   */
  getSlideStyle(index, currentIndex, type) {
    const { offsetPercentage } = this.state;
    const totalSlides = this.getTotalSlidesCount() - 1;
    let zIndex = 1;

    // current index has more zIndex so slides wont fly by toggling infinite
    if (index === currentIndex) {
      zIndex = 3;
    } else if (index === this.state.previousIndex) {
      zIndex = 2;
    }

    if (type === 'slide') {
      const basetranslateX = -100 * currentIndex;
      let translateX = basetranslateX + (index * 100) + offsetPercentage;

      if (this.props.infinite) {
        if (currentIndex === 0 && index === totalSlides) {
          // make the last slide the slide before the first
          translateX = -100 + offsetPercentage;
        } else if (currentIndex === totalSlides && index === 0) {
          // make the first slide the slide after the last
          translateX = 100 + offsetPercentage;
        }
      }

      const translate3d = `translate3d(${translateX}%, 0, 0)`;

      return {
        WebkitTransform: translate3d,
        MozTransform: translate3d,
        msTransform: translate3d,
        OTransform: translate3d,
        transform: translate3d,
        zIndex,
      };
    }
    return {};
  }

  /**
   * @desc Iterates through the slide items and generates the appropriate markup for
   * 1) each slide 2) navigation. Each slide and navigation is added to an independent
   * array used for rendering.
   * @param {array} items used to generate slides/navigation.
   * @return {object} contains two objects each holding an array assignment
   * for each slide and navigation
   */
  getCarouselElement(items, carouselElementType) {
    const { currentIndex, viewportWidth, style } = this.state;
    const slideList = [];
    const bulletList = [];

    _map(items, (item, index) => {
      const alignment = this.getAlignmentClassName(index, currentIndex);
      const slideMarkup = (
        <div
          key={index}
          className={`${this.props.className}-slide${alignment}`}
          style={_extend(this.getSlideStyle(index, currentIndex, 'slide'), style)}
          onClick={this.props.onClick}
          onTouchStart={this.props.onClick}
        >
          <div className={`${this.props.className}-imageBox`}>
            <ImageItem
              images={item.image}
              viewportWidth={viewportWidth}
              handleOnLoad={this.props.onImageLoad}
            />
          </div>
          <TextItem
            className={`${this.props.className}-content`}
            tag={item.category}
            target={item.link}
            title={item.title}
            description={item.description}
            date={item.date}
            location={item.location}
          />
        </div>
      );

      if (this.props.lazyLoad) {
        if (alignment) {
          slideList.push(slideMarkup);
        }
      } else {
        slideList.push(slideMarkup);
      }

      bulletList.push(
        <li
          key={index}
          className={`${this.props.className}-nav-item`}
        >
          <NavButton
            className={`${this.props.className}-nav-button`}
            handleOnTouchStart={(event) => this.slideToIndex.call(this, index, event)}
            handleOnClick={(event) => this.slideToIndex.call(this, index, event)}
            currentIndex={currentIndex}
            position={index}
            label={item.shortTitle}
          />
        </li>
      );
    });

    if (carouselElementType === 'slides') {
      return slideList;
    }
    if (carouselElementType === 'nav') {
      return bulletList;
    }

    return {
      slideList,
      bulletList,
    };
  }

  /**
   * @desc Helper method used to obtain the amount of carousel items.
   * @return {number} number representation for the amount of carousel items
   */
  getTotalSlidesCount() {
    return this.props.items.length;
  }

  /**
   * @desc Boolean flag used to check if there are at least 2 carousel items to iterate or
   * if the showNav boolean is set.
   * @return {boolean} true or false.
   */
  canNavigate() {
    return this.getTotalSlidesCount() >= 2 && this.props.showNav;
  }

  /**
   * @desc Boolean flag that verifies if the current carousel index or the infinite flag
   * is valid in order to slide left.
   * @return {boolean} true or false.
   */
  canSlideLeft() {
    if (this.props.infinite) {
      return true;
    }
    return this.state.currentIndex > 0;
  }

  /**
   * @desc Boolean flag that verifies if the current carousel index or the infinite flag
   * is valid in order to slide right.
   * @return {boolean} true or false.
   */
  canSlideRight() {
    if (this.props.infinite) {
      return true;
    }
    return this.state.currentIndex < this.getTotalSlidesCount() - 1;
  }

  /**
   * @desc Helper method used trigger slideToIndex with minus 1 slide
   */
  slideLeft() {
    this.slideToIndex(this.state.currentIndex - 1);
  }

  /**
   * @desc Helper method used trigger slideToIndex with plus 1 slide
   */
  slideRight() {
    this.slideToIndex(this.state.currentIndex + 1);
  }

  /**
   * @desc Iterates through the slide items and generates the appropriate markup for
   * 1) each slide 2) navigation. Each slide and navigation is added to an independent
   * array used for rendering.
   * @param {array} items used to generate slides/navigation.
   * @return {object} contains two objects each holding an array assignment
   * for each slide and navigation
   */
  slideToIndex(index, event) {
    if (event) {
      event.preventDefault();
      if (this.intervalId) {
        // user triggered event while carousel is playing, reset interval.
        this.pause(false);
        this.play(false);
      }
    }

    const slideCount = this.getTotalSlidesCount() - 1;
    let currentIndex = index;

    if (index < 0) {
      currentIndex = slideCount;
    } else if (index > slideCount) {
      currentIndex = 0;
    }

    this.setState({
      previousIndex: this.state.currentIndex,
      currentIndex,
      offsetPercentage: 0,
      style: {
        transition: 'transform .45s ease-out',
      },
    });
  }

  play(enableCallbackFunc = true) {
    if (this.intervalId) {
      return;
    }

    const { slideInterval } = this.props;

    this.intervalId = window.setInterval(
      () => {
        if (!this.props.infinite && !this.canSlideRight()) {
          this.pause();
        } else {
          this.slideToIndex(this.state.currentIndex + 1);
        }
      },
      (slideInterval > MIN_INTERVAL) ? slideInterval : MIN_INTERVAL
    );

    if (this.props.onPlay && enableCallbackFunc) {
      this.props.onPlay(this.state.currentIndex);
    }
  }

  pause(enableCallbackFunc = true) {
    if (this.intervalId) {
      window.clearInterval(this.intervalId);
      this.intervalId = null;
    }

    if (this.props.onPause && enableCallbackFunc) {
      this.props.onPause(this.state.currentIndex);
    }
  }

  /**
   * @desc Handles updating the viewportWidth's state only if an instance of
   * carouselComponent is initialized.
   */
  handleResize() {
    if (this.carouselComponent) {
      this.setState({ viewportWidth: this.carouselComponent.offsetWidth });
    }
  }

  /**
   * @desc Handles the keyboard LEFT/RIGHT arrow interaction to fire off the
   * appropriate slide method.
   * @param {object} Native JavaScript event object.
   */
  handleKeyDown(event) {
    const LEFT_ARROW = 37;
    const RIGHT_ARROW = 39;
    const key = parseInt(event.keyCode || event.which || 0, 10);

    if (!this.intervalId) {
      if (key === LEFT_ARROW && this.canSlideLeft()) {
        this.slideLeft();
      }

      if (key === RIGHT_ARROW && this.canSlideRight()) {
        this.slideRight();
      }
    }
  }

  /**
   * @desc Handles updating the boolean flag when an onSwiped event occurs.
   */
  handleOnSwiped(ev, x, y, isFlick) {
    this.setState({ isFlick });
  }

  /**
   * @desc Handles the swipe event by assigning the appropriate index to
   * fire off the sliteToIndex method.
   * @param {number} represents the slide index.
   */
  handleOnSwipedTo(index) {
    let slideTo = this.state.currentIndex;

    if (Math.abs(this.state.offsetPercentage) > 30 || this.state.isFlick) {
      slideTo += index;
    }

    if (index < 0) {
      if (!this.canSlideLeft()) {
        slideTo = this.state.currentIndex;
      }
    } else {
      if (!this.canSlideRight()) {
        slideTo = this.state.currentIndex;
      }
    }

    this.slideToIndex(slideTo);
  }

  /**
   * @desc Handles updating the state's offsetPercentage value upon a
   * swipe event.
   * @param {number} Index number to be decremented/incremented.
   * @param {object} Synthetic Touch Event object.
   * @param {number} Touch position on viewport
   */
  handleSwiping(index, _, delta) {
    const offsetPercentage = (index * (delta / this.state.viewportWidth * 100));
    this.setState({ offsetPercentage, style: {} });
  }

  /**
   * @desc Returns the proper DOM Navigation elements generated by
   * getSlideElements() method only if there is enough items to navigate.
   */
  renderNavigation() {
    if (this.canNavigate()) {
      const navList = this.getCarouselElement(this.props.items, 'nav');
      return (
        <div className={`${this.props.className}-nav`}>
          <ul className={`${this.props.className}-nav-list`}>
            {navList}
          </ul>
        </div>
      );
    }
    return null;
  }

  /**
   * @desc Returns the proper DOM for each Swipeable Carousel slide only
   * if the items array is not empty.
   * @param {array} - Carousel items to be iterated.
   */
  renderSlides(items) {
    if (!_isEmpty(items)) {
      const slideList = this.getCarouselElement(items, 'slides');
      const handleOnSwipingLeft = this.handleSwiping.bind(this, -1);
      const handleOnSwipingRight = this.handleSwiping.bind(this, 1);
      const handleOnSwipedLeft = this.handleOnSwipedTo.bind(this, 1);
      const handleOnSwipedRight = this.handleOnSwipedTo.bind(this, -1);

      return (
        <Swipeable
          className={`${this.props.className}-slides`}
          key="swipeableCarousel"
          onSwipingLeft={handleOnSwipingLeft}
          onSwipingRight={handleOnSwipingRight}
          onSwiped={this.handleOnSwiped}
          onSwipedLeft={handleOnSwipedLeft}
          onSwipedRight={handleOnSwipedRight}
        >
          {slideList}
        </Swipeable>
      );
    }

    const {
      tag,
      title,
      description,
    } = this.props.error;

    return (
      <div className="error">
        <h2>{tag}</h2>
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    );
  }

  render() {
    return (
      <div
        ref={i => (this.carouselComponent = i)}
        className={this.props.className}
      >
        {this.renderSlides(this.props.items)}
        {this.renderNavigation()}
      </div>
    );
  }
}

CarouselComponent.propTypes = {
  items: React.PropTypes.array.isRequired,
  showNav: React.PropTypes.bool,
  autoPlay: React.PropTypes.bool,
  lazyLoad: React.PropTypes.bool,
  infinite: React.PropTypes.bool,
  defaultImage: React.PropTypes.string,
  lang: React.PropTypes.string,
  className: React.PropTypes.string,
  startIndex: React.PropTypes.number,
  slideInterval: React.PropTypes.number,
  onSlide: React.PropTypes.func,
  onPause: React.PropTypes.func,
  onPlay: React.PropTypes.func,
  onClick: React.PropTypes.func,
  onImageLoad: React.PropTypes.func,
  error: React.PropTypes.object,
};

CarouselComponent.defaultProps = {
  lang: 'en',
  className: 'hpCarousel',
  items: [],
  showNav: true,
  autoPlay: false,
  lazyLoad: false,
  infinite: false,
  startIndex: 0,
  slideInterval: 3000,
  error: {
    tag: 'ERROR',
    title: 'Something has gone wrong.',
    description: 'We\'re sorry. Information isn\'t available for this feature.',
  },
};

export default CarouselComponent;
