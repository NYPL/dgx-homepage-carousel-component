import React from 'react';

/**
  * @desc Verifies that the property value exists and returns it.
  * Otherwise, returns the default value.
  * @param {Object} param - Object containing the nested value
  * @param {String} param - String representation of the language property name.
  * @returns {String} param - Returns the string value for the given property match.
  */
function getProperty(prop = {}, lang, fallback) {
  const defaultValue = fallback || null;
  return prop && prop[lang] && prop[lang].text ? prop[lang].text : defaultValue;
}

/**
  * @desc Verifies that the string value exists and returns it. Otherwise, returns a hash.
  * @param {String} param - String representation of the target property.
  * @returns {String} param - Returns the string value for the given property match.
  */
function getString(string, fallback) {
  const defaultValue = fallback || null;
  return (typeof string === 'string' && string !== '') ? string : defaultValue;
}

const TextItem = ({
  className,
  target,
  tag,
  title,
  description,
  location,
  date,
  lang,
}) => {
  const content = {
    url: getString(target, '#'),
    tag: getProperty(tag, lang, 'ERROR'),
    title: getProperty(title, lang, 'Something has gone wrong.'),
    desc: getProperty(description, lang),
    date: getProperty(date, lang),
    location: getString(location),
  };

  return (
    <div className={className}>
      <a
        className={`${className}-tag`}
        href={content.url}
      >
        <h3>{content.tag}</h3>
      </a>
      <a
        className={`${className}-title`}
        href={content.url}
      >
        <h2>{content.title}</h2>
      </a>
      {
        (content.date || content.location) ?
          <a className={`${className}-details`} href={content.url}>
            {content.date ?
              <span className={`${className}-date`}>{content.date}</span> : null
            }
            {content.location ?
              <span className={`${className}-location`}>{content.location}</span> : null
            }
          </a>
        :
          <a
            className={`${className}-description`}
            href={content.url}
          >
            {content.desc ? <p>{content.desc}</p> : null}
          </a>
      }
    </div>
  );
};

TextItem.propTypes = {
  className: React.PropTypes.string,
  lang: React.PropTypes.string,
  target: React.PropTypes.string,
  location: React.PropTypes.string,
  tag: React.PropTypes.object,
  title: React.PropTypes.object,
  date: React.PropTypes.object,
  description: React.PropTypes.object,
};

TextItem.defaultProps = {
  className: 'textItem',
  lang: 'en',
};

export default TextItem;
