# Homepage Carousel Component

This is the component for homepage that works as the hero and displays 
full width images.

## Usage

    import CarouselComponent from 'dgx-homepage-carousel-component';

	<CarouselComponent content={YourContent} errorType={YourErrorType} />

## Props

- `id`: ID of the carousel component (String, default: "CarouselComponent")
- `className`: The class name of the carousel component (String, default: "CarouselComponent")
- `content` : The data of the carousel component. It should be passed by the carousel component's parent component or be fetched by client side AJAX. (Array, default: [])
- `lang`: Language. Not used, but provided for future internationalization
  (String, default: "en")

## Demo

After cloning the repo:

    npm install
    npm start

Then load [http://localhost:3000](http://localhost:3000) in your browser.